 
document.addEventListener("DOMContentLoaded", function () {
    
    const playerName = document.getElementById("player-name");
    const tableRows = document.getElementById("table-rows");
    const selectedDifficulty = document.getElementById("difficulty");
    const cheatCheckbox = document.getElementById("cheat");
    const StartButton = document.getElementById("start-button");
    const radioButtons = document.querySelectorAll(".color-option input[type='radio']");
    const allButtons = document.getElementsByClassName("buttons");
    const submitGuessBtn = document.getElementById("guess-submittion-button");
    const timerElement = document.getElementById('timer');
    const saveButton = document.getElementById('save-game-sessions');
    const clearAllButton = document.getElementById('clear-all');
    const loadButton = document.getElementById('load-game-sessions');
    const playersInfoArray = [];


    /**
    * Validates the player's input based on predefined criteria.
    * 
    * This function checks if the 'playerName', 'StartButton', and 'submitGuessBtn' elements are present.
    * If they are, it validates the player's name to ensure it's alphanumeric and between 4 to 12 characters long.
    * If the validation passes, it changes the player name input's background color to green, enables the StartButton,
    * and adds an event listener to the StartButton to enable the submitGuessBtn and change its background color upon click.
    * If the validation fails, it disables the StartButton and changes the player name input's background color to orange.
    * Additionally, it sets the background color of all buttons to a grey color.
    * This function assumes that the 'enable_disable_Button' and 'changeButtonBackgroundColor' functions are defined elsewhere.
    */
    function changeButtonBackgroundColor() {
        let selectedColor = document.querySelector(".color-option [type='radio']:checked").value;
        if(selectedColor){
            let finalColor = document.getElementById("final-selected-color");
            for (let i = 0; i < allButtons.length; i++) {
                allButtons[i].style.backgroundColor = selectedColor;
                finalColor.style.backgroundColor = selectedColor;
                finalColor.innerText = selectedColor;
            }
        }else{
            let finalColor = document.getElementById("final-selected-color");
            for (let i = 0; i < allButtons.length; i++) {
                allButtons[i].style.backgroundColor = selectedColor;
                finalColor.style.backgroundColor = selectedColor;
                finalColor.innerText = selectedColor;
            } 
        } 
    }

    /**
    * Validates player input, specifically the player name and a Start button.
    * If the player name is valid, it changes the background color of the player name input,
    * enables the Start button, adds an event listener to the Start button, and calls the
    * 'changeButtonBackgroundColor' function. If the input is invalid, it disables the Start
    * button and sets the background color accordingly.
    */
    function validatePlayerInput() {
        
        
        if (playerName && StartButton && submitGuessBtn) {
            let playerNameValue = playerName.value;
            let alphanumericPattern = /^[a-zA-Z0-9]+$/;
    
            if (playerNameValue.length >= 4 && playerNameValue.length <= 12 && alphanumericPattern.test(playerNameValue)) {
                playerName.style.backgroundColor = "#90ee90";
                enable_disable_Button(StartButton, false);
                StartButton.addEventListener('click', () => {
                    enable_disable_Button(submitGuessBtn, false);
                    changeButtonBackgroundColor();
                });
            } else {
                enable_disable_Button(StartButton, true);
                playerName.style.backgroundColor = "#ffa500";
                document.querySelectorAll("button").forEach(button => {
                    button.style.backgroundColor = "#a9a9a9";
                });
            }
        }
        
    }
    //note from tannaz:  there is a bug that if the number of tiles to be selected is 2 red and the player also choose 2 tiles but green the success rate will be zero. 
    //I tried to fix the problem using dominent color function but it caused more bugs.  soorry :(( pls pls play the game without trying to figure out the bugs :)

    /**
    * Calculates the success rate of tile selection in a game.
    *
    * @returns {string} The rounded success rate as a string (e.g., "0.75").
    */
    function calculateSuccess() {
        let numberToGuess = document.getElementById("number-of-tiles-to-select").innerText;
        let matches = numberToGuess.match(/\d+/);
        let numericValue = parseInt(matches[0]); 
        const selectedTiles = countTiles();
        // Calculate success rate
        const successRate = selectedTiles / numericValue;
        const roundedSuccessRate = successRate.toFixed(2);
        if (parseFloat(roundedSuccessRate) == 1) { 
            let hurray = new Audio('../audio/success.mp3');
            hurray.play();
        } else if (parseFloat(roundedSuccessRate) < 0.5) { 
            let boo = new Audio('../audio/boo.mp3');
            boo.play();
        }
        return roundedSuccessRate; 
    };

    let seconds = 0;
    let timerInterval;
    let timerRunning = false;

    /**
     * Updates the timer display each second.
     * Calculates minutes and seconds from a global 'seconds' variable, formats them, 
     * and updates 'timerElement's text content with this time. Increments 'seconds' by 1.
     * Assumes 'seconds' and 'timerElement' are defined externally.
     * 
     * @returns {string} Formatted time as "minutes:seconds".
     */
    function updateTimer() {
        let minutes = Math.floor(seconds / 60);
        let remainingSeconds = seconds % 60;
        let displayTime = `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
        timerElement.textContent = displayTime;
        seconds++;
        return displayTime;
    };

    
    /**
     * Event listener for the 'click' event on 'submitGuessBtn'.
     * Disables the button, changes its background color, and processes the player's guess.
     * It calculates and records the number of tile clicks, the current time, and the success rate,
     * updates the player's information in 'playersInfoArray', and updates the display table.
     * Also stops the timer if it's running.
     * 
     * Assumes the existence of various functions (e.g., 'countTiles', 'updateTimer', etc.) and 
     * global variables (e.g., 'playerName', 'tableRows', 'selectedDifficulty', etc.) used within.
     */
    submitGuessBtn.addEventListener('click', function (){
        enable_disable_Button(submitGuessBtn, true);
        submitGuessBtn.style.backgroundColor = "#a9a9a9";

        let num = countTiles();
        let time = updateTimer();
        let rate = calculateSuccess();
        collectPlayerInformation(playerName, tableRows, selectedDifficulty, cheatCheckbox, playersInfoArray);
        
            playersInfoArray[playersInfoArray.length - 1].dur = time;
            playersInfoArray[playersInfoArray.length - 1].numOfclicks = num;
            playersInfoArray[playersInfoArray.length - 1].successRate = rate*100+"%";
        
        createTableFromArrayObjects(playersInfoArray);
        if (timerRunning) {
            clearInterval(timerInterval);
            timerRunning = false;
        }
        calculateSuccess();
    });

    /**
     * Event listener for the 'click' event on 'StartButton'.
     * Toggles the game timer on and off. If the timer is running, it stops and resets it. 
     * If not, it starts the timer from 0 and updates it every second.
     * 
     * Assumes the existence of global variables 'timerRunning', 'timerInterval', 'timerElement', 
     * and 'seconds', as well as the 'updateTimer' function.
     */

    StartButton.addEventListener('click', () => {
        if (timerRunning) {
            clearInterval(timerInterval);
            timerRunning = false;
        } else {
            timerElement.textContent = '0:00';
            seconds = 0;
            timerInterval = setInterval(updateTimer, 1000);
            timerRunning = true;
        }
    });

    for (let i = 0; i < radioButtons.length; i++) {
        radioButtons[i].addEventListener('change', changeButtonBackgroundColor);
    }
    if (playerName) {
        playerName.addEventListener('input', validatePlayerInput);
    }
    validatePlayerInput();
    
    let tableParentContainer = document.getElementById("actualGame");

    /**
     * Event listener for the 'click' event on 'tableParentContainer'.
     * Toggles a border class on table cells (TD elements) when clicked.
     * Adds the 'tdBorder' class if it's not present, and removes it if it is.
     * 
     * the 'tdBorder' class is defined in CSS. 
     * when each tile is clicked the border bocomes yellow
     */
    tableParentContainer.addEventListener('click', function(event) {
        let target = event.target;
        
        if (target.tagName === 'TD') {
            if (target.classList.contains('tdBorder')) {
                target.classList.remove('tdBorder');
            } else {
                target.classList.add('tdBorder');
            }
        }
    });
    


    /**
     * Event listener for the 'click' event on 'saveButton'.
     * it saves all the data in the tables to the local storage
     * 
     * the 'saveAllGames2Storage' function is defined in web-storage.js and takes 'playersInfoArray' as an argument.
     */
    saveButton.addEventListener('click', function() {
        saveAllGames2Storage(playersInfoArray);
    });


    /**
     * Event listener for the 'click' event on 'clearAllButton'.
     * Clears all game data from storage.
     * 
     * the 'clearAllGamesFromStorage' function is defined in web-storage.js and requires no arguments.
     */
    clearAllButton.addEventListener('click',clearAllGamesFromStorage);



    /**
     * Event listener for the 'click' event on 'loadButton'.
     * Loads all game data from storage.
     * 
     * the 'readAllGamesFromStorage' function is defined in webstorage.js and requires no arguments.
     */
    loadButton.addEventListener('click', function() {
        readAllGamesFromStorage();
    });
    
    
});




