/**
 * Storing generated colors into an array according to the difficulty
 *
 * @param difficulty - game difficulty
 * @param size - game grid size
 * @return array2dColors
 */
function generateColorByDifficulty(difficulty, size){
    let array2dColors = [];

    for(let i = 0; i < size; i++){
        array2dColors.push([]);
        for(let j = 0; j < size; j++){
            
            if(difficulty == 'easy'){
                array2dColors[i].push(generateColor());
            }
            else if(difficulty == 'medium'){
                array2dColors[i].push(generateColor(80));
                
            }
            else if(difficulty == 'difficult'){
                array2dColors[i].push(generateColor(50));
            }
            else if(difficulty == 'brutal'){
                array2dColors[i].push(generateColor(10));
            }
            else{
                console.log('generate color error');
            }
        }
    }
    return array2dColors;
}


/**
 * generating color using Math.random in the interval
 *
 * @param interval. If interval is null, it generates a random number.
 * @return rgb(r,g,b).
 * */
function generateColor(interval){
    let randomNum = () => Math.round(Math.random() * 255);

    let r = randomNum();
    let min = r - interval;
    let max = r + interval;
    
    if(min < 0){
        min = 0
    }
    if(max > 255){
        max = 255
    }
    let randomNumMinMax = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);
    let g = randomNumMinMax(min, max);
    let b = randomNumMinMax(min, max);

    if(typeof interval !== 'number'){
        g = randomNum();
        b = randomNum();
    }
    
    return `rgb(${r}, ${g}, ${b})`;
}