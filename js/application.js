document.addEventListener('DOMContentLoaded', () => {
    const startPuzzleButton = document.getElementById('start-button');
    const submitGuessBtn = document.getElementById("guess-submittion-button");
    let tableParentContainer = document.getElementById("actualGame");

    if(tableParentContainer.childElementCount == 0){
        enable_disable_Button(submitGuessBtn, true);
        submitGuessBtn.style.backgroundColor = "#a9a9a9";
    }
    /**
     * Event listener for the 'click' event on 'startButton'.
     * Initializes the puzzle game when clicked. It includes changing the color of the submitGuessBtn,
     * retrieving user inputs for game size, difficulty, and cheat option, generating a color array based
     * on these inputs, creating the game table, and setting up the cheat functionality.
     * 
     * the functions like 'generateColorByDifficulty', 'generateTable', 
     * and 'showCheat', as well as certain DOM elements like 'submitGuessBtn', 'gameSizeInput', 
     * 'gameDifficultyInput', and 'cheat' are defined in utilities.js.
     */
    startPuzzleButton.addEventListener('click', () => {
        
        
        // Change submit guesses button colors
        let buttonsColor = document.getElementsByClassName("buttons")[0].style.backgroundColor;
        submitGuessBtn.style.backgroundColor = buttonsColor;


        //getting user input
        const gameSizeInput = document.getElementById('table-rows');
        const gameDifficultyInput = document.querySelector('select');
        const cheat = document.getElementById('cheat');

        //generating the color array
        const colorsArray = generateColorByDifficulty(gameDifficultyInput.value, gameSizeInput.value);

        //generating table + cheats depending on checked or not
        generateTable(gameSizeInput.value, colorsArray);

    
        showCheat(colorsArray, cheat.checked);

        cheat.addEventListener('click', function(){
            showCheat(colorsArray, cheat.checked);
        });

        submitGuessBtn.addEventListener('click', () => {
            showCheat(colorsArray, true);
        })
    

    }

    )});
