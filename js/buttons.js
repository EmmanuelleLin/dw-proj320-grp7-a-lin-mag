const tableRows = document.getElementById("table-rows");
const selectedColor = document.querySelector(".color-option [type='radio']:checked");
const selectedDifficulty = document.getElementById("difficulty");
const cheatCheckbox = document.getElementById("cheat");
    const playerName = document.getElementById("player-name");
    const StartButton = document.getElementById("start-button");

    const submitGuessBtn = document.getElementById("guess-submittion-button");
    let timerInterval;
    let timerRunning = false;
    let seconds = 0;

    StartButton.addEventListener('click', () => {
        if (timerRunning) {
            clearInterval(timerInterval);
            timerRunning = false;
        } else {
            seconds = 0;
            timerDisplay = updateTimer(); // Get the timer display string using the function
            timerInterval = setInterval(() => {
                timerDisplay = updateTimer(); // Update the timer display
            }, 1000);
            timerRunning = true;
        }
    });
    
submitGuessBtn.addEventListener('click', function (){
        enable_disable_Button(submitGuessBtn, true);
        submitGuessBtn.style.backgroundColor = "#a9a9a9";

        let num = countTiles();
        let time = updateTimer();
        collectPlayerInformation(playerName, tableRows, selectedColor, selectedDifficulty, cheatCheckbox, playersInfoArray);
        if (playersInfoArray.length > 0) {
            playersInfoArray[playersInfoArray.length - 1].numOfclicks = num;
            playersInfoArray[playersInfoArray.length - 1].duration = time;
        }
        createTableFromArrayObjects(playersInfoArray);
        if (timerRunning) {
            clearInterval(timerInterval);
            timerRunning = false;
        }
        
    });
/*StartButton.addEventListener('click', () => {
        
    
        if (timerRunning) {
            clearInterval(timerInterval);
            timerRunning = false;
        } else {
            
            seconds = 0;
            timerInterval = setInterval(updateTimer, 1000);
            timerRunning = true;
        }
    });*/