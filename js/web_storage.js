let saveButton = document.getElementById('save-game-sessions');


/**
 * Saves all game data to local storage.
 * Serializes the players' information array into JSON and stores it in local storage under the key 'savedGames'.
 * If the array is empty, it disables the save button and exits the function
 * After successfully saving, it updates a message element to notify the user of the save.
 * 
 * 'enable_disable_Button' is a function that is defined in utilities.js
 * 
 * @param {Object[]} playersInfoArray - The array of player information objects to be saved.
 */
function saveAllGames2Storage(playersInfoArray) {
    if (playersInfoArray.length === 0) {
        enable_disable_Button(saveButton, true);
        return;
    }
    let gamesJSON = JSON.stringify(playersInfoArray);
    localStorage.setItem('savedGames', gamesJSON);
    document.getElementById('storage-message').innerText = "All games have been saved to local storage...";
}
/**
 * Clears all saved game data from local storage
 * Removes the 'savedGames' item from local storage and updates a message element
 * to notify the user that all game sessions have been removed.
 * 
 */
function clearAllGamesFromStorage() {
    localStorage.removeItem('savedGames');
    document.getElementById('storage-message').innerText = "All game sessions have been removed from local storage...";
}
/**
 * Retrieves and displays all saved game data from local storage.
 * Reads the 'savedGames' item from local storage, parses it into an array of game data,
 * and uses this data to create a table. If no saved games are found, it updates a message element
 * to notify the user that no saved game sessions are available.
 * 
 * 'createTableFromArrayObjects' is a function defined in utilities.js
 */
function readAllGamesFromStorage() {
    const gamesJSON = localStorage.getItem('savedGames');

    if (gamesJSON) {
        const gamesArray = JSON.parse(gamesJSON);

        createTableFromArrayObjects(gamesArray);
    } else {
        document.getElementById('storage-message').innerText = "No saved game sessions found in local storage.";
    }
}


    
