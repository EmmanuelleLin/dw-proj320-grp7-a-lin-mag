/**
 * disables or unables button
 * 
 * @param {HTMLButtonElement} button 
 * @returns {boolean} /button.disabled - true or false
 */
function enable_disable_Button(button, disabled) {
    button.disabled = disabled;
}

/**
 * create an element by receiving the tag, the parent element, and the element id
 *
 * @param {HTMLElement} tag 
 * @param {HTMLElement} parent 
 * @param {...any} elemId 
 * @returns {HTMLElement} /newElement - the created element
 */
function createMyElement(tag, parent, elemId){
    let newElement = document.createElement(tag)
    newElement.id = elemId
    parent.append(newElement);
    return newElement;
}

/**
 * create a table from an array of Objects being the score table
 * 
 * @param {Array} arrayOfObjects 
 * @param {HTMLElement} parent 
 * @param {...any} table_id 
 */
function createTableFromArrayObjects(arrayOfObjects){
    let parent = document.getElementById('status-table');
    
    let checkIfEmpty = parent.querySelector('table');
    if(!(checkIfEmpty == null)){
        checkIfEmpty.remove();
    }
    //table elements
    const table = createMyElement('table', parent, 'score-table');
    const contentThead = ['Player', 'dur', 'clicks', 'Level', 'Cheat', 'Success', 'rowXcol'];
    const tableHead = createMyElement('thead', table, 'table-head');
    let header = createMyElement('tr', tableHead);
    //headers with buttons
    for(let i in contentThead){
        let newTd = createMyElement('td', header, contentThead[i]);
        let newButton = createMyElement('button', newTd, `${contentThead[i]}Button`);
        newButton.className = 'sort';
        newButton.textContent = contentThead[i];
        newButton.addEventListener('click', () => sortTable(newButton.id, arrayOfObjects));
    }

    //tbody
    const tableBody = createMyElement('tbody', table, 'table-body');
    arrayOfObjects.forEach((row) => {
        let gameRecord = createMyElement('tr', tableBody);
        for (const key in row) {
            let newTd = createMyElement('td', gameRecord);
            newTd.textContent = row[key];
        }
    });
}

/**
 * Calculates the number of times a selected color appears in a 2D array of colors.
 * It iterates through each color, extracts its dominant color, and compares it with
 * the selected color. Increments a count if a match is found.
 * 
 * extractDominantColor is a function that returns the dominant color of an input color
 * 
 * @param {string[][]} colors - 2D array of color strings.
 * @param {string} selectedColor - The color to count within the array.
 * @returns {number} The count of how many times 'selectedColor' appears in 'colors'.
 */
function calculateNum2Guess(colors, selectedColor) {
    let num2Guess = 0;
    for (let i = 0; i < colors.length; i++) {
        for (let j = 0; j < colors[i].length; j++) {
            let extractedColor = extractDominantColor(colors[i][j]);
            if (extractedColor === selectedColor) {
                num2Guess++;
            }
        }
    }
    return num2Guess;
}

/**
 * generate table using size, parent tag, and the array of generated colors
 * 
 * @param {Number} size 
 * @param {HTMLElement} parent 
 * @param {Array} colors 
 */
function generateTable(size, colors) {
    let checkIfEmpty = document.getElementById('actualGame').children[0];
    if (!(checkIfEmpty == null)) {
        checkIfEmpty.remove();
    }
    let selectedColor = document.getElementById("final-selected-color").textContent;
    const parent = document.getElementById('actualGame');
    const table = createMyElement('table', parent, 'game-table');
    const tableBody = createMyElement('tbody', table, 'table-body');

    const num2Guess = calculateNum2Guess(colors, selectedColor); 

    for (let i = 0; i < size; i++) {
        let row = createMyElement('tr', tableBody);
        for (let j = 0; j < size; j++) {
            let td = createMyElement('td', row, `td${i}${j}`);
            td.style.backgroundColor = colors[i][j];
            td.textContent = redGreenOrBlue123(colors[i][j]);
        }
    }

    document.getElementById('number-of-tiles-to-select').innerHTML = `${num2Guess} ${selectedColor}`;
}
/**
 * displays cheat in every td using array of generated colors, and depending on if the cheat checkbox is checked or not
 * 
 * @param {Array} colors 
 * @param {boolean} cheatOrNot 
 */
function showCheat(colors, cheatOrNot){
    let pThereOrNo = document.querySelectorAll('.cheats');
    if(!cheatOrNot || !(pThereOrNo == null)){
       document.querySelectorAll('.cheats').forEach(element => element.remove());
    }
    for(let i = 0; i < colors.length; i++){
        for(let j = 0; j < colors.length; j++){
            let td = document.getElementById(`td${i}${j}`);
            if(cheatOrNot){
                let text = createMyElement('p', td, `text${i}${j}`);
                text.className = 'cheats';
                text.textContent = colors[i][j];
            }
        }
    }   
}

/**
 * Generating 0, 1, 2 according to the biggest number
 * 
 * @param {Array} colors 
 * @returns {Array} /array of 1, 2, and 3 for referencing red: 0, green: 1, and blue: 2
 */
function redGreenOrBlue123(color){
    let oneTwoOrThree = 0;
    let arrayRGB = color.substring(4,color.length-1);
    arrayRGB = arrayRGB.split(","); 

        if(parseInt(arrayRGB[0]) <= parseInt(arrayRGB[2]) && parseInt(arrayRGB[1]) <= parseInt(arrayRGB[2])){
            oneTwoOrThree = 2; 
        }
        else if(parseInt(arrayRGB[0]) <= parseInt(arrayRGB[1]) && parseInt(arrayRGB[2]) <= parseInt(arrayRGB[1])){
            oneTwoOrThree = 1; 
        }
        else{
            oneTwoOrThree = 0;
        
    }
    return oneTwoOrThree;
}

/**
 * Extracts the dominant color (red, green, or blue) from an RGB color string.
 * The function parses an RGB color string, compares the RGB values, and determines
 * the dominant color based on the highest value among red, green, and blue components.
 * 
 * @param {string} color - An RGB color string in the format "rgb(r, g, b)".
 * @returns {string} The dominant color ('red', 'green', or 'blue') from the RGB string.
 */
function extractDominantColor(color){
    if (typeof color !== 'string' || !color.startsWith('rgb(')) {
        console.error('Invalid color format:', color);
        return null; // Or handle this case as needed
    }

    // Extracting RGB values
    let arrayRGB = color.match(/\d+/g); // This will extract all number groups

    if (!arrayRGB || arrayRGB.length !== 3) {
        console.error('Invalid RGB values:', color);
        return null; // Or handle this case as needed
    }

    // Convert the extracted values to integers
    arrayRGB = arrayRGB.map(Number);

    // Logic to determine the dominant color
    if (arrayRGB[0] <= arrayRGB[2] && arrayRGB[1] <= arrayRGB[2]) {
        return "blue";
    } else if (arrayRGB[0] <= arrayRGB[1] && arrayRGB[2] <= arrayRGB[1]) {
        return "green";
    } else if (arrayRGB[1] <= arrayRGB[0] && arrayRGB[2] <= arrayRGB[0]) {
        return "red";
    }
}


let isAscending = true;
let lastButtonId = null;

/**
 * Sorts an array of objects based on a specified column and toggles sorting order.
 * The function determines the sort column based on the button ID and applies a corresponding
 * sorting function. It toggles between ascending and descending order for repeated clicks on the same button.
 * 
 * 'createTableFromArrayObjects' is a function for rendering the table, and 'lastButtonId' 
 * and 'isAscending' are global variables tracking the last sorted column and sort order, respectively.
 * 'compareNumbers' is a helper function for numerical comparisons, and 'levelToNumber' converts level strings to numerical values.
 * 
 * @param {string} buttonId - The ID of the button clicked, determining the sort column.
 * @param {Object[]} arrObj - The array of objects to be sorted.
 */
function sortTable(buttonId, arrObj) {
    // Toggle sort order if the same button is clicked again
    if (buttonId === lastButtonId) {
        isAscending = !isAscending;
    } else {
        lastButtonId = buttonId;
        isAscending = true; // Reset to default order for new column
    }

    // Define sort functions
    const sortFunctions = {
        'PlayerButton': (a, b) => a.player.localeCompare(b.player),
        'durButton': (a, b) => compareNumbers(parseFloat(a.dur), parseFloat(b.dur)),
        'clicksButton': (a, b) => compareNumbers(parseInt(a.clicks, 10), parseInt(b.clicks, 10)),
        'LevelButton': (a, b) => compareNumbers(levelToNumber(a.level), levelToNumber(b.level)),
        'CheatButton': (a, b) => a.cheat.localeCompare(b.cheat),
        'SuccessButton': (a, b) => compareNumbers(parseFloat(a.successRate), parseFloat(b.successRate)),
        'rowXcolButton': (a, b) => compareNumbers(parseInt(a.size, 10), parseInt(b.size, 10))
    };

    function compareNumbers(a, b) {
        return a - b;
    }    
    
    if (sortFunctions[buttonId]) {
        arrObj.sort((a, b) => isAscending ? sortFunctions[buttonId](a, b) : -sortFunctions[buttonId](a, b));
        createTableFromArrayObjects(arrObj);
    }
}

/**
 * Converts a difficulty level string to a corresponding numerical value.
 * Maps 'easy' to 1, 'medium' to 2, 'hard' to 3, and 'brutal' to 4. The function 
 * is designed to be used in sorting or comparison contexts where numerical values
 * are required to represent difficulty levels.
 * 
 * @param {string} level - The difficulty level as a string ('easy', 'medium', 'hard', 'brutal').
 * @returns {number} The numerical representation of the difficulty level.
 */        
function levelToNumber(level) {
    switch (level) {
        case 'easy': return 1;
        case 'medium': return 2;
        case 'hard': return 3;
        case 'brutal': return 4;
    }
};


/**
 * Collects and stores player information in an array.
 * Gathers data from various input elements and checkboxes, then creates an object 
 * representing the player's information, including their name, difficulty level, cheat usage, 
 * and other game-related metrics. This object is then added to an array of players' information.
 * 
 * @param {HTMLElement} playerNameInput - The input element containing the player's name.
 * @param {HTMLElement} tableRowsInput - The input element containing the number of table rows.
 * @param {HTMLElement} selectedDifficulty - The select element containing the chosen difficulty level.
 * @param {HTMLElement} cheatCheckbox - The checkbox element indicating if cheats were used.
 * @param {Object[]} playersInfoArray - The array to store player information objects.
 */
function collectPlayerInformation(playerNameInput, tableRowsInput, selectedDifficulty, cheatCheckbox, playersInfoArray) {
    let playerInformation = {
        player: playerNameInput.value,
        dur: undefined,
        numOfclicks: undefined,
        level: undefined,
        cheat:undefined,
        successRate: undefined,
        size: undefined,
    }
    

    if (selectedDifficulty) {
      playerInformation.level = selectedDifficulty.value;
    }
  
    if (cheatCheckbox.checked) {
      playerInformation.cheat = "Yes";
    } else {
      playerInformation.cheat = "No";
    }
    
    playerInformation.size = tableRowsInput.value;
    
  
    playersInfoArray.push(playerInformation);
  }


/**
 * Counts the number of table cells (tiles)that has the css style of tdBorder
 * use the `reduce` method to iterate over all 'td' elements in the document and 
 * accumulates a count for each cell containing the 'tdBorder' class.
 * 
 * @returns {number} The count of 'td' elements with the 'tdBorder' class.
 */
function countTiles() {
    const allTiles = document.querySelectorAll("td");
    return Array.from(allTiles).reduce((count, td) => td.classList.contains("tdBorder") ? count + 1 : count, 0);
}